const eye = document.getElementsByClassName('icon-password')
eyeArr = [...eye]

const submitButton = document.getElementById('button')
const enterPasswordForm = document.getElementById('enter-password')
const checkPasswordForm = document.getElementById('check-password')
const passwordForm = document.querySelector('.password-form')

eyeArr.forEach((item) => {
    item.addEventListener('click', (e) => {
        const currentBlock = e.target;

        if (currentBlock.classList.value.includes('fa-eye-slash')) {
            currentBlock.classList.remove('fa-eye-slash');
            currentBlock.classList.add('fa-eye');

            if (e.target.id === 'enter-eye') {
                enterPasswordForm.type = 'password';
            } else {
                checkPasswordForm.type = 'password'
            }

        } else {
            currentBlock.classList.remove('fa-eye');
            currentBlock.classList.add('fa-eye-slash');

            if (e.target.id === 'enter-eye') {
                enterPasswordForm.type = 'text';
            } else {
                checkPasswordForm.type = 'text'
            }
        }
    })
})



const errorText = document.createElement('p');
errorText.innerText = 'Нужно ввести одинаковые значения';
errorText.classList.add('error-text');
errorText.classList.add('is-hidden');
submitButton.before(errorText);

submitButton.addEventListener('click', (e) => {
    e.preventDefault()

    if (enterPasswordForm.value === checkPasswordForm.value) {
        errorText.classList.add('is-hidden')
        alert('You are welcome')
    } else {
        errorText.classList.remove('is-hidden');
    }
})