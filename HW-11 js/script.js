const button = document.getElementsByClassName('btn')
const buttons = [...button]

document.addEventListener('keydown', (event) => {
   buttons.forEach((item) => {
       if (event.key === item.innerText || event.code === 'Key' + item.innerText){
           item.classList.add('active')
       } else {
           item.classList.remove('active')
       }
   })
})





