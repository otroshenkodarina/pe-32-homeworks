document.addEventListener('DOMContentLoaded', () => {
    const button = document.querySelector('.change-theme')
    const page = document.getElementById('theme')
    const theme = localStorage.getItem('theme')

    if (theme === 'light') {
        page.classList.remove('dark')
        page.classList.add('light')
    } else {
        page.classList.add('dark')
        page.classList.remove('light')
    }

    button.addEventListener('click', (e) => {
        page.classList.toggle('light')
        page.classList.toggle('dark')

        if (page.classList.contains('dark')) {
            localStorage.setItem('theme', 'dark')
        } else if (page.classList.contains('light')) {
            localStorage.setItem('theme', 'light')
        }
    })
})