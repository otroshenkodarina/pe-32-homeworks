let number = +prompt('enter an integer')

while (isNaN(number) || !Number.isInteger(number) || number === 0){
    number = +prompt('try again')
}

for (let i = 1; i > 0 && i <= number; i++) {
    if (i % 5 === 0) {
        console.log(i)
    } else if (number >= 0 && number <= 4){
        console.log('Sorry, no numbers')
    }
}

for (let i = -1; i < 0 && i >= number; i--) {
    if (i % 5 === 0) {
        console.log(i)
    } else if (number <= 0 && number >= -4){
        console.log('Sorry, no numbers')
    }
}

