let n = +prompt('enter a number')
while (isNaN(n) || n == ''){
    n = +prompt('try again', n)
}

function f(n) {
    if (n === 0){
        return 0;
    }
    else if (n === 1){
        return 1;
    }
    else if (n < 0){
        return f(n + 2) - f(n + 1)
    }
    else{
        return f(n - 1) + f(n - 2);
    }
}

alert(f(n));