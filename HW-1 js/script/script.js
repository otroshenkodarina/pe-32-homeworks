let userName = ''

while (true) {

    userName = prompt('Enter your name', userName);

    if (userName === '') {
        alert('Try again, userName is empty!');
    } else if (!isNaN(+userName)) {
        alert('Try again, userName should contain letters');
    } else {
        break;
    }
}

let userAge

while (true) {
    userAge = prompt('Enter your age', userAge)

    if (Number(userAge) === 0) {
        alert('Try again, userAge is empty')
    } else if (isNaN(Number(userAge))) {
        alert('age is a number, try again')
    } else {
        break
    }
}

if (Number(userAge) < 18) {
    alert('You are not allowed to visit this website')
}
else if (Number(userAge) >= 18 && Number(userAge) <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome, ' + userName)
    } else {
        alert('You are not allowed to visit this website')
    }
} else if (Number(userAge) > 22) {
    alert('Welcome, ' + userName)
}

