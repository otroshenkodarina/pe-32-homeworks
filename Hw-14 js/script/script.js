
function slowScroll (id) {
    const offset = 0
    $('html, body').animate({
        scrollTop: $(id).offset().top - offset
    }, 1000)
    return false
}

$(window).scroll(function () {
    const height = document.documentElement.clientHeight
    const button = document.querySelector('.button')

    if ($(this).scrollTop() > height) {
        button.classList.add('active')
    }
});

$(window).scroll(function () {
    const height = document.documentElement.clientHeight
    const button = document.querySelector('.button')

    if ($(this).scrollTop() < height) {
        button.classList.remove('active')
    }
});

$('#toggle').click(function() {
    $('.animated').slideToggle(1000)
    $('.article8').css('display', 'flex');
});
