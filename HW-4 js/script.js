function createNewUser(name, surname) {
    const newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function () {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLowerCase()
        },
        setFirstName: function (newFirstName) {
            delete this.lastName
            this.lastName = newFirstName;
        },
        setLastName: function (newLastName) {
            delete this.lastName
            this.lastName = newLastName;
        },
    }
    Object.defineProperty(newUser, 'firstName', {
        writable: false
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    })

    return newUser
}

const newUser = createNewUser(prompt('enter your name'), prompt('enter your surname'))


console.log(newUser.getLogin())

