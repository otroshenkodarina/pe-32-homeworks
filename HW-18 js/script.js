// let obj = {
//     a: 1,
//     b: {
//         c: 2,
//     },
// }

// let newObj = JSON.parse(JSON.stringify(obj))
//
// obj.b.c = 20
// console.log(obj)
// console.log(newObj)

function deepClone(obj) {
    let cloneObj = {};
    for(let i in obj) {
        if (obj[i] instanceof Object) {
            cloneObj[i] = deepClone(obj[i]);
            continue;
        }
        cloneObj[i] = obj[i];
    }
    return cloneObj;
}

console.log(deepClone({
    a: 1,
    b: {
        c: 2,
    },
}))