function createNewUser(name, surname, birthday) {
    const newUser = {
        firstName: name,
        lastName: surname,
        birthday: birthday,
        getAge: function() {
            let date = new Date()
            let dateArray = this.birthday.split('.')
            let exactDate = new Date(`${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`)
            let birthdayYear = exactDate.getFullYear()
            let age = date.getFullYear() - birthdayYear
            if (date.getMonth() < exactDate.getMonth()){
                age = age - 1
            } else if (date.getMonth() === exactDate.getMonth() && date.getDate() < exactDate.getDate()){
                age = age - 1
            }
            return age
        },
        getPassword: function() {
            let dateArray = this.birthday.split('.')
            let exactDate = new Date(`${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`)
            let birthdayYear = exactDate.getFullYear()

            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthdayYear

        },
        getLogin: function () {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLowerCase()
        },
        setFirstName: function (newFirstName) {
            delete this.lastName
            this.lastName = newFirstName;
        },
        setLastName: function (newLastName) {
            delete this.lastName
            this.lastName = newLastName;
        },
    }
    Object.defineProperty(newUser, 'firstName', {
        writable: false
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    })

    return newUser
}

const newUser = createNewUser(prompt('enter your name'), prompt('enter your surname'), prompt('enter date of your birthday'))


console.log(newUser.getAge())
console.log(newUser.getPassword())

