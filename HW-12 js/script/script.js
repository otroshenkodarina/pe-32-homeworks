function showPictures(interval, currentIndex) {
    const stopButton = document.querySelector('.stop-btn')

    const timerID = setTimeout(() => {
        const pictures = document.querySelectorAll('.image-to-show')
        const picturesArray = [...pictures]

        if (currentIndex === picturesArray.length) {
            currentIndex = 0
        }

        picturesArray.forEach((item) => {
            item.classList.add('hidden')
        })

        const currentPicture = picturesArray[currentIndex]
        currentPicture.classList.remove('hidden')

        stopButton.removeEventListener('click', () => {
            clearTimeout(timerID);
        })
        showPictures(interval, currentIndex + 1)
    }, interval)

    stopButton.addEventListener('click', () => {
        clearTimeout(timerID);
    })

}

showPictures(3000, 1)


const continueButton = document.querySelector('.continue-btn')

continueButton.addEventListener('click', () => {
    let newIndex = null
    const pictures = document.querySelectorAll('.image-to-show')
    const picturesArray = [...pictures]

    picturesArray.forEach((item, key) => {
        if (!item.classList.value.includes('hidden')) {
            newIndex = key
        }
    })

    showPictures(3000, newIndex + 1)
})




















