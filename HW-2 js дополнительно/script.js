let num1 = +prompt('enter number 1')
while (isNaN(num1) || num1 == '') {
    num1 = +prompt('try again num 1')
}

let num2 = +prompt('enter number 2')
while (isNaN(num2) || num2 == '') {
    num2 = +prompt('try again num 2')
}


for (let i = num1; i <= num2; i++) {
    let primeNum = true;

    for (let j = 2; j < i; j++) {
        if (i <= 1 || i % j === 0) {
            primeNum = false;
            break;
        }
    }
    if (i > 1 && primeNum) {
        console.log('RESULT --> ', i)
    }
}

