const button = document.querySelector('.button')

const input = document.createElement('input')
input.placeholder = 'Введите диаметр круга'
input.type = 'number'
input.classList.add('input')
button.after(input)

const circleContainer = document.createElement('div')
circleContainer.classList.add('circle-block')
document.body.prepend(circleContainer)

function randomInt(max, min) {
    return Math.ceil(Math.random() * (max - min) + min)
}

function randomColor() {
    return `rgb(${randomInt(1, 254)}, ${randomInt(1, 254)}, ${randomInt(1, 254)}`
}

function createCircle() {
    const circle = document.createElement('div')
    circle.style.width = input.value + 'px'
    circle.style.height = input.value + 'px'
    circle.style.clipPath = 'circle(50% at 50% 50%)'
    circle.style.backgroundColor = randomColor()
    circle.classList.add('circle')
    circleContainer.append(circle)
}

button.addEventListener('click', (e) => {
    input.style.display = 'block'
})

input.addEventListener('blur', (e) => {
    circleContainer.style.display = 'grid'
    for (let i = 0; i < 100; i++) {
        createCircle()
    }
    input.remove()
    button.remove()
})

document.addEventListener('click', (e) => {
    if (e.target.classList.contains('circle')){
        document.querySelector('.circle').remove()
    }
})