function getSumOfListItems(list) {
    return list.reduce((acc, val) => acc + val);
}

function getResultInTime(speedOfWork, taskList, dateOfDeadline) {
    const sumSpeedOfWork = getSumOfListItems(speedOfWork)
    const sumTaskList = getSumOfListItems(taskList)

    const numberOfDaysForWork = sumTaskList / sumSpeedOfWork

    const now = new Date()

    const newDateOfDeadline = new Date(dateOfDeadline)
    const numberOfDayLeft = (newDateOfDeadline - now) / (60 * 60 * 24 * 1000)

    if (numberOfDayLeft > numberOfDaysForWork) {
        const numberOfDaysToDeadline = parseInt(numberOfDayLeft - numberOfDaysForWork)
        alert(`Все задачи будут успешно выполнены за ${numberOfDaysToDeadline} дней до наступления дедлайна!`)
    } else if (numberOfDayLeft < numberOfDaysForWork){
        const additionalHours = parseInt((numberOfDaysForWork - numberOfDayLeft) * 8)
        alert(`Команде разработчиков придется потратить дополнительно ${additionalHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    } else if (parseInt(numberOfDayLeft) === parseInt(numberOfDaysForWork)){
        alert(`Все задачи будут выполнены вовремя`)
    }
}


getResultInTime([3, 4, 8, 5], [20, 50, 30, 90, 555], '2021-06-16');