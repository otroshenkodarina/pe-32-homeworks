let num1 = +prompt('enter number 1')
while (isNaN(num1) || num1 == '') {
    num1 = +prompt('enter again number 1', num1)
}

let num2 = +prompt('enter number 2')
while (isNaN(num2) || num2 == '') {
    num2 = +prompt('enter again number 2', num2)
}

let operation = prompt('enter an operation "*", "/", "+", "-"')
while (operation !== '*' && operation !== '/' && operation !== '+' && operation !== '-') {
    operation = prompt('try again', operation)
}

function f(num1, num2, operation) {
    if (operation === '*') {
        alert(num1 * num2)
    } else if (operation === '/') {
        alert(num1 / num2)
    } else if (operation === '+') {
        alert(num1 + num2)
    } else if (operation === '-') {
        alert(num1 - num2)
    }
}

f(num1, num2, operation)

