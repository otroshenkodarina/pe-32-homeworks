function makeList(array, parent = document.body) {
    const ulBlock = document.createElement('ul');

    array.map(function (item) {
        const listItem = document.createElement('li')

        if (Array.isArray(item)) {
            makeList(item, listItem)
        } else {
            listItem.innerText = `${item}`
        }

        ulBlock.append(listItem)
    })

    parent.append(ulBlock)
}


function deleteCode() {
    document.body.innerText = ''
}

function countSeconds(seconds) {
    let amountOfSeconds = document.createElement('p')
    amountOfSeconds.innerText = `00:00:0${seconds}`

    setInterval(() => {
        amountOfSeconds.innerText = `00:00:0${seconds - 1}`
        if (seconds > 0) {
            seconds = seconds - 1
        } else {
            seconds = 0
        }
    }, 1000)

    document.body.prepend(amountOfSeconds)
}


countSeconds(3)
makeList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])
setTimeout(deleteCode, 4000)

