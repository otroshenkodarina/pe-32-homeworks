const MIN_SUCCESS_SCHOLARSHIP_ASSIGNED = 7
const MIN_SUCCESS_TRANSFERRED_TO_THE_NEXT_COURSE = 0

function createNewStudent(name, surname) {
    let student = {
        name: name,
        lastName: surname,
        tabel: {},
        getCountBadGrades: function () {
            let badGrades = 0
            for (let grade in this.tabel) {
                if (this.tabel[grade] < 4) {
                    badGrades++
                }
            }
            return badGrades
        },
        getCountAverageGrades: function (){
            let amountOfGrades = 0
            let sumOfGrades = 0
            for (let grade in this.tabel){
                sumOfGrades += +this.tabel[grade]
                amountOfGrades ++
            }
            return sumOfGrades / amountOfGrades
        },
        isTransferredToTheNextCourse: function () {
            return this.getCountBadGrades() === MIN_SUCCESS_TRANSFERRED_TO_THE_NEXT_COURSE;
        },
        isScholarshipAssigned: function () {
            return this.getCountAverageGrades() > MIN_SUCCESS_SCHOLARSHIP_ASSIGNED
        },
        getMessageTransferredToTheNextCourse: function () {
            if (this.isTransferredToTheNextCourse()) {
                alert('Студент переведен на следующий курс.')
            }
        },
        getMessageScholarshipAssigned: function () {
            if (this.isScholarshipAssigned()){
                alert('Студенту назначена стипендия.')
            }
        },
    }

    let studentSubject = prompt('enter subject')
    let studentGrade = prompt('enter your grade')

    while (checkIsValidSubject() && checkIsValidGrade()) {
        student.tabel[studentSubject] = +studentGrade
        studentSubject = prompt('enter a subject again')

        if (checkIsValidSubject()) {
            studentGrade = prompt('enter your grade')
        }
    }

    function checkIsValidSubject() {
        return studentSubject !== null && studentSubject !== "";
    }

    function checkIsValidGrade() {
        return studentGrade !== null && studentGrade !== '' && !isNaN(+studentGrade)
    }

    return student
}

const newStudent = createNewStudent(prompt('enter your name'), prompt('enter your surname'))

console.log('newStudent --> ', newStudent)

newStudent.getMessageTransferredToTheNextCourse()
newStudent.getMessageScholarshipAssigned()


